package hu.unideb.rft.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "cart")
public class Cart implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String uri;

    @Column(nullable = false)
    private Long userId;

    @Column(nullable = false)
    private String label;

    @ElementCollection
    private List<String> ingredients;

    private String image;
}
