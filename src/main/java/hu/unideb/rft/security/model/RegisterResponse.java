package hu.unideb.rft.security.model;

import hu.unideb.rft.enums.ErrorCodes;
import hu.unideb.rft.enums.ErrorMessages;
import lombok.Data;

@Data
public class RegisterResponse {
    private String errorMessage;
    private int errorCode;

    public RegisterResponse(ErrorMessages errorMessages, ErrorCodes errorCodes){
        this.errorCode = errorCodes.getNumber();
        this.errorMessage = errorMessages.getMessage();
    }
}
