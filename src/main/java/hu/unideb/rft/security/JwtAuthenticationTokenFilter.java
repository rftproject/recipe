package hu.unideb.rft.security;

import hu.unideb.rft.security.model.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter{
	
    private final UserDetailsService authService;
    private final JwtTokenUtil jwtTokenUtil;
    private final String tokenHeader;

    @Autowired
    public JwtAuthenticationTokenFilter(JwtTokenUtil jwtTokenUtil, @Value("${jwt.header}") String tokenHeader, UserDetailsService authService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.tokenHeader = tokenHeader;
        this.authService = authService;
    }
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, 
									HttpServletResponse response, 
									FilterChain filterChain) throws ServletException, IOException {
		
		final String header = request.getHeader(this.tokenHeader);
		String username = null;
		String authToken = null;
		if(header != null && header.startsWith("Bearer ")) {
			authToken = header.substring(7);
			try {
				username = jwtTokenUtil.getUsernameFromToken(authToken);
			}catch(Exception e) {
				log.error("" + e);
			}
		}

		if(username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = this.authService.loadUserByUsername(username);

			if(jwtTokenUtil != null && jwtTokenUtil.validateToken(authToken, userDetails)) {
				UsernamePasswordAuthenticationToken auth =
								new UsernamePasswordAuthenticationToken(userDetails, 
								null, 
								userDetails.getAuthorities());
				auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}

		filterChain.doFilter(request, response);
	}
}
