package hu.unideb.rft.api;

import hu.unideb.rft.api.entity.BaseResponse;
import hu.unideb.rft.api.entity.Hit;
import hu.unideb.rft.api.entity.Recipe;
import hu.unideb.rft.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Component
public class RestClientService {

    private static final String BASE_URL = "https://api.edamam.com/search";
    private static final String APP_KEY = "30374ce6484e1bdf415b3dcf0330154c";
    private static final String APP_ID = "075089ee";
    private static final int JERSEY_CLIENT_CONNECT_TIMEOUT = 15_000;
    private static final int JERSEY_CLIENT_READ_TIMEOUT = 15_000;

    public List<Recipe> downloadRecipes(String queryParams, int from, int to) throws BusinessException {
        Client client = getClient();

        LinkedList<Recipe> recipes = new LinkedList<>();
        try {
            Response response = client.target(BASE_URL)
                    .queryParam("app_id", APP_ID)
                    .queryParam("app_key", APP_KEY)
                    .queryParam("q", queryParams)
                    .queryParam("from", from)
                    .queryParam("to", to)
                    .request(MediaType.APPLICATION_JSON)
                    .get();

            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new BusinessException(String.format("REST válasz hiba: status: %d, message: %s", response.getStatusInfo().getStatusCode(),
                        response.getStatusInfo().getReasonPhrase()));
            }

            response.bufferEntity();

            BaseResponse baseResponse = response.readEntity(BaseResponse.class);
            if (baseResponse.getCount() == 0) {
                throw new BusinessException("Nem található '" + queryParams + "'-hoz recept");
            }

            for (Hit hit : baseResponse.getHits()) {
                recipes.add(hit.getRecipe());
            }
        } finally {
            client.close();
        }

        return recipes;
    }

    public Recipe getRecipeById(String id) throws BusinessException {
        Client client = getClient();
        
        String encodedId;
        try {
            encodedId = URLEncoder.encode(id, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException(String.format("Recipe id cannot be encoded: %s", id), e);
        }

        List<Recipe> recipes;
        try {
            Response response = client.target(BASE_URL)
                    .queryParam("app_id", APP_ID)
                    .queryParam("app_key", APP_KEY)
                    .queryParam("r", encodedId)
                    .request(MediaType.APPLICATION_JSON)
                    .get();

            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new BusinessException(String.format("REST válasz hiba: status: %d, message: %s", response.getStatusInfo().getStatusCode(),
                        response.getStatusInfo().getReasonPhrase()));
            }

            response.bufferEntity();

            recipes = response.readEntity(new GenericType<List<Recipe>>() {});
            if (recipes.size() == 0) {
                throw new BusinessException("Nem található '" + id + "'-hoz recept");
            }
        } finally {
            client.close();
        }

        return recipes.get(0);
    }

    private Client getClient() {
        Client client = ClientBuilder.newClient();
        client.property(ClientProperties.CONNECT_TIMEOUT, JERSEY_CLIENT_CONNECT_TIMEOUT);
        client.property(ClientProperties.READ_TIMEOUT, JERSEY_CLIENT_READ_TIMEOUT);
        return client;
    }
}
