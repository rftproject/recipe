package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "label",
    "quantity",
    "unit"
})
@Data
public class Nutrient implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("label")
    public String label;
    
    @JsonProperty("quantity")
    public Double quantity;
    
    @JsonProperty("unit")
    public String unit;
}
