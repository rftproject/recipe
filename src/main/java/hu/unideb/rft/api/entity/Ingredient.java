package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "text",
    "weight"
})
@Data
public class Ingredient implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("text")
    private String text;

    @JsonProperty("weight")
    private Double weight;
}
