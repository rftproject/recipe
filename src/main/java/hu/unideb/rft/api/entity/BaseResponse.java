package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "q",
    "from",
    "to",
    "params",
    "more",
    "count",
    "hits"
})
@Data
public class BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("q")
    private String q;

    @JsonProperty("from")
    private Integer from;

    @JsonProperty("to")
    private Integer to;

    @JsonProperty("params")
    private Params params;

    @JsonProperty("more")
    private Boolean more;

    @JsonProperty("count")
    private Integer count;

    @JsonProperty("hits")
    private List<Hit> hits;
}
