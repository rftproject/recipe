package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "uri",
    "label",
    "image",
    "source",
    "url",
    "shareAs",
    "yield",
    "dietLabels",
    "healthLabels",
    "cautions",
    "ingredientLines",
    "ingredients",
    "calories",
    "totalWeight",
    "totalTime",
    "totalNutrients",
    "totalDaily",
    "digest"
})
@Data
@ToString(of = "label")
public class Recipe implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("uri")
    private String uri;
    
    @JsonProperty("label")
    private String label;
    
    @JsonProperty("image")
    private String image;
    
    @JsonProperty("source")
    private String source;

    @JsonProperty("url")
    private String url;

    @JsonProperty("shareAs")
    private String shareAs;

    @JsonProperty("yield")
    private Double yield;

    @JsonProperty("dietLabels")
    private List<String> dietLabels;

    @JsonProperty("healthLabels")
    private List<String> healthLabels;

//    @JsonProperty("cautions")
    @JsonIgnore
    private List<Object> cautions;   //TODO: not specified type

    @JsonProperty("ingredientLines")
    private List<String> ingredientLines;

    @JsonProperty("ingredients")
    private List<Ingredient> ingredients;

    @JsonProperty("calories")
    private Double calories;

    @JsonProperty("totalWeight")
    private Double totalWeight;

    @JsonProperty("totalTime")
    private Double totalTime;

    @JsonIgnore
    private TotalNutrients totalNutrients;

    @JsonIgnore
    private TotalNutrients totalDaily;

    @JsonIgnore
    private List<Digest> digest;

}
