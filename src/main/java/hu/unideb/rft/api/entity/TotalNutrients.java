package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "ENERC_KCAL",
    "FAT",
    "FASAT",
    "FATRN",
    "FAMS",
    "FAPU",
    "CHOCDF",
    "FIBTG",
    "SUGAR",
    "PROCNT",
    "CHOLE",
    "NA",
    "CA",
    "MG",
    "K",
    "FE",
    "ZN",
    "P",
    "VITA_RAE",
    "VITC",
    "THIA",
    "RIBF",
    "NIA",
    "VITB6A",
    "FOLDFE",
    "FOLFD",
    "VITB12",
    "VITD",
    "TOCPHA",
    "VITK1"
})
@Data
public class TotalNutrients implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("ENERC_KCAL")
    private Nutrient eNERCKCAL;

    @JsonProperty("FAT")
    private Nutrient fAT;

    @JsonProperty("FASAT")
    private Nutrient fASAT;

    @JsonProperty("FATRN")
    private Nutrient fATRN;
    
    @JsonProperty("FAMS")
    private Nutrient fAMS;
    
    @JsonProperty("FAPU")
    private Nutrient fAPU;
    
    @JsonProperty("CHOCDF")
    private Nutrient cHOCDF;
    
    @JsonProperty("FIBTG")
    private Nutrient fIBTG;
    
    @JsonProperty("SUGAR")
    private Nutrient sUGAR;
    
    @JsonProperty("PROCNT")
    private Nutrient pROCNT;
    
    @JsonProperty("CHOLE")
    private Nutrient cHOLE;
    
    @JsonProperty("NA")
    private Nutrient nA;
    
    @JsonProperty("CA")
    private Nutrient cA;
    
    @JsonProperty("MG")
    private Nutrient mG;
    
    @JsonProperty("K")
    private Nutrient k;
    
    @JsonProperty("FE")
    private Nutrient fE;
    
    @JsonProperty("ZN")
    private Nutrient zN;
    
    @JsonProperty("P")
    private Nutrient p;
    
    @JsonProperty("VITA_RAE")
    private Nutrient vITARAE;
    
    @JsonProperty("VITC")
    private Nutrient vITC;
    
    @JsonProperty("THIA")
    private Nutrient tHIA;
    
    @JsonProperty("RIBF")
    private Nutrient rIBF;
    
    @JsonProperty("NIA")
    private Nutrient nIA;
    
    @JsonProperty("VITB6A")
    private Nutrient vITB6A;
    
    @JsonProperty("FOLDFE")
    private Nutrient fOLDFE;
    
    @JsonProperty("FOLFD")
    private Nutrient fOLFD;
    
    @JsonProperty("VITB12")
    private Nutrient vITB12;
    
    @JsonProperty("VITD")
    private Nutrient vITD;
    
    @JsonProperty("TOCPHA")
    private Nutrient tOCPHA;
    
    @JsonProperty("VITK1")
    private Nutrient vITK1;
}
