package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "sane",
    "q",
    "from",
    "app_key",
    "to",
    "app_id"
})
@Data
public class Params implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("sane")
    private List<Object> sane;

    @JsonProperty("q")
    private List<String> q;

    @JsonProperty("from")
    private List<String> from;

    @JsonProperty("app_key")
    private List<String> appKey;

    @JsonProperty("to")
    private List<String> to;

    @JsonProperty("app_id")
    private List<String> appId;
}