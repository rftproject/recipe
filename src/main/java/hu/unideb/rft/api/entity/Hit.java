package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "recipe",
    "bookmarked",
    "bought"
})
@Data
public class Hit implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("recipe")
    private Recipe recipe;

    @JsonProperty("bookmarked")
    private Boolean bookmarked;

    @JsonProperty("bought")
    private Boolean bought;

}
