package hu.unideb.rft.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "label",
    "tag",
    "schemaOrgTag",
    "total",
    "hasRDI",
    "daily",
    "unit",
    "sub"
})
@Data
public class Digest implements Serializable {
    private final static long serialVersionUID = 1L;

    @JsonProperty("label")
    private String label;

    @JsonProperty("tag")
    private String tag;

    @JsonProperty("schemaOrgTag")
    private Object schemaOrgTag;

    @JsonProperty("total")
    private Double total;

    @JsonProperty("hasRDI")
    private Boolean hasRDI;

    @JsonProperty("daily")
    private Double daily;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("sub")
    private List<Sub> sub;

}
