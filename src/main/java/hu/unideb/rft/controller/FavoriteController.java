package hu.unideb.rft.controller;

import hu.unideb.rft.model.Favorite;
import hu.unideb.rft.service.FavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("/api/favorites")
@CrossOrigin
public class FavoriteController {
    private FavoriteService favoriteService;

    @Autowired
    public FavoriteController(FavoriteService favoriteService) {
        this.favoriteService = favoriteService;
    }

    @PostMapping("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void addFavorite(@RequestBody Favorite favorite) {
        favoriteService.addFavorite(favorite);
    }



    @PostMapping("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void deleteFavoriteById(@RequestBody Long id) {
        favoriteService.deleteById(id);
    }

    @PostMapping("/delete-by-uri-and-userid")
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void deleteByUriAndUserId(@RequestBody Favorite favorite) {
        favoriteService.deleteByUriAndUserId(favorite.getUri(), favorite.getUserId());
    }

    @GetMapping("/{userId}")
    public List<Favorite> getFavoritesByUserId(@PathVariable("userId") Long userId) {
        return favoriteService.getFavoritesByUserId(userId);
    }

    @PostMapping("/isfavorite")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public Boolean isFavorite(@RequestBody Favorite favorite) {
        return favoriteService.isFavorite(favorite.getUri(), favorite.getUserId());
    }
}
