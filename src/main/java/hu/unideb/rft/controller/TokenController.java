package hu.unideb.rft.controller;

import hu.unideb.rft.enums.ErrorCodes;
import hu.unideb.rft.enums.ErrorMessages;
import hu.unideb.rft.model.User;
import hu.unideb.rft.security.model.JwtAuthResponse;
import hu.unideb.rft.security.model.JwtTokenUtil;
import hu.unideb.rft.security.model.JwtUser;
import hu.unideb.rft.security.model.RegisterResponse;
import hu.unideb.rft.service.UserService;
import hu.unideb.rft.security.model.AuthReq;
import hu.unideb.rft.security.model.RegReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.net.URI;

@RestController
@RequestMapping("/token")
@CrossOrigin
@Slf4j
public class TokenController {
    
    @Value("${jwt.header}")
    String tokenHeader;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserDetailsService userDetailsService;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public TokenController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, 
                           @Qualifier("authService") UserDetailsService userDetailsService,
                           UserService userService, 
                           PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(value = "/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity createAuthenticationToken(@RequestBody AuthReq authRequest, HttpServletRequest request) throws AuthenticationException {
            authenticate(authRequest.getUsername(), authRequest.getPassword());

            final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());
            User user = userService.findByUsername(authRequest.getUsername());

            final String token = jwtTokenUtil.generateToken(userDetails);
            String id = Long.toString(user.getId());

            // Return the token
            return ResponseEntity.ok(new JwtAuthResponse(id, token));
    }
    
     @PostMapping("/register")
     @Produces(MediaType.APPLICATION_JSON)
     @Consumes(MediaType.APPLICATION_JSON)
     @ResponseBody
    public RegisterResponse registerProfile(@Valid @RequestBody RegReq regReq) {

        if (userService.existsByUsername(regReq.getUsername())) {
            log.info("Token refresh: [ERR] - USED USERNAME");
            return new RegisterResponse(ErrorMessages.WRONG_USERNAME, ErrorCodes.WRONG_USERNAME);
        }

        if (userService.existsByEmail(regReq.getEmail())) {
            log.info("Token refresh: [ERR] - USED EMAIL");
            return new RegisterResponse(ErrorMessages.WRONG_EMAIL, ErrorCodes.WRONG_EMAIL);
        }

        User user = new User(regReq.getUsername(),regReq.getName(), regReq.getPassword(), regReq.getEmail(), null);

        User result = userService.save(user);

         URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/user/{id}").buildAndExpand(result.getId()).toUri();
         log.info("User registration: [OK]");
         return new RegisterResponse(ErrorMessages.OK, ErrorCodes.OK);
    }
    
    @GetMapping(value = "/refresh")
    public RegisterResponse refreshAndGetAuthenticationToken(HttpServletRequest request) {

        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            log.info("Token refresh: [OK]");
            return new RegisterResponse(ErrorMessages.OK, ErrorCodes.OK);
        } else {
            log.error("Token refresh: [ERR]");
            return new RegisterResponse(ErrorMessages.TOKEN_REFRESH_ERROR, ErrorCodes.TOKEN_REFRESH_ERROR);
        }
    }

    private void authenticate(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (AuthenticationException e) {
            log.error("" + e);
        }
    }
}
