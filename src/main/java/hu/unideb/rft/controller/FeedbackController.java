package hu.unideb.rft.controller;

import hu.unideb.rft.model.Feedback;
import hu.unideb.rft.service.mail.FeedbackSender;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.ValidationException;

@RestController
@RequestMapping("/api/feedback")
@CrossOrigin
public class FeedbackController {

    private FeedbackSender feedbackSender;

    public FeedbackController(FeedbackSender feedbackSender) {
        this.feedbackSender = feedbackSender;
    }

    @PostMapping
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void sendFeedback(@RequestBody Feedback feedback, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            try {
                throw new ValidationException("Feedback has errors; Can not send feedback;");
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        }

        this.feedbackSender.sendFeedback(
                feedback.getEmail(),
                feedback.getName(),
                feedback.getFeedback());
    }
}
