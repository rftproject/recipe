package hu.unideb.rft.controller;

import hu.unideb.rft.api.entity.Recipe;
import hu.unideb.rft.model.RecipeByIdRequest;
import hu.unideb.rft.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.awt.*;
import java.util.List;

@RestController
@RequestMapping("/api/recipes")
@CrossOrigin
public class RecipeController {

    private final RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping("/{ingredients}/{from}/{to}")
    public List<Recipe> getRecipesFromIngredients(@PathVariable("ingredients") String ingredients,
                                                  @PathVariable("from") int from,
                                                  @PathVariable("to") int to) {
        return recipeService.getRecipes(ingredients, from, to);
    }

    @PostMapping("/id")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public Recipe getRecipeById(@RequestBody RecipeByIdRequest request) {
        return recipeService.getRecipeById(request.getId());
    }
}
