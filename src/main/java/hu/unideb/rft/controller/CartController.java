package hu.unideb.rft.controller;

import hu.unideb.rft.model.Cart;
import hu.unideb.rft.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("/api/cart")
@CrossOrigin
public class CartController {
    private CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }


    @PostMapping("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void addCart(@RequestBody Cart  cart) {
        cartService.addCart(cart);
    }

    @PostMapping("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void deleteCartById(@RequestBody Long id) {
        cartService.deleteById(id);
    }

    @PostMapping("/delete-by-uri-and-userid")
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public void deleteByUriAndUserId(@RequestBody Cart cart) {
        cartService.deleteByUriAndUserId(cart.getUri(), cart.getUserId());
    }

    @GetMapping("/{userId}")
    public List<Cart> getCartsByUserId(@PathVariable("userId") Long userId) {
        return cartService.getCartByUserId(userId);
    }

    @PostMapping("/isincart")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseBody
    public Boolean isInCart(@RequestBody Cart cart) {
        return cartService.isInCart(cart.getUri(), cart.getUserId());
    }
}
