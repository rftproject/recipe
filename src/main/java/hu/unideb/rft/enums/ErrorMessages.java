package hu.unideb.rft.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorMessages {

    UNAUTHORIZED("Access denied!"),
    WRONG_USERNAME("This user name is taken!"),
    WRONG_EMAIL("This email address is taken!"),
    OK("Successful registration!"),
    UNKNOWN_ERROR("UNKNOWN_ERROR"),
    TOKEN_REFRESH_ERROR("Token refresh error!");

    private String message;

    @Override
    public String toString() {
        return "ErrorMessages{" +
                "message='" + message + '\'' +
                '}';
    }
}
