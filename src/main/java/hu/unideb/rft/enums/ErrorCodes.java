package hu.unideb.rft.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorCodes {

    UNAUTHORIZED(401),
    WRONG_USERNAME(-1),
    WRONG_EMAIL(-2),
    OK(200),
    UNKNOWN_ERROR(-999),
    TOKEN_REFRESH_ERROR(-3);

    private Integer number;

    @Override
    public String toString() {
        return "ErrorCodes{" +
                "number=" + number +
                '}';
    }
}
