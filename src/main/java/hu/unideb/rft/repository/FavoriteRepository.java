package hu.unideb.rft.repository;

import hu.unideb.rft.model.Favorite;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FavoriteRepository extends CrudRepository<Favorite, Long> {
    Iterable<Favorite> findByUserId(Long userId);
    Optional<Favorite> findByUriAndUserId(String uri, Long userId);
    void deleteByUriAndUserId(String uri, Long userId);
}
