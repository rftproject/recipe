package hu.unideb.rft.repository;

import hu.unideb.rft.model.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {
    Iterable<Cart> findByUserId(Long userId);
    Optional<Cart> findByUriAndUserId(String uri, Long userId);
    void deleteByUriAndUserId(String uri, Long userId);
}
