package hu.unideb.rft.service;

import hu.unideb.rft.model.Cart;
import hu.unideb.rft.repository.CartRepository;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartService {
    private final CartRepository cartRepository;

    @Autowired
    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }


    public void addCart(Cart cart) {
        Optional<Cart> c = cartRepository.findByUriAndUserId(cart.getUri(), cart.getUserId());
        if (!c.isPresent()) {
            cartRepository.save(cart);
        }
    }

    public void deleteById(Long id) {
        cartRepository.deleteById(id);
    }

    public void deleteByUriAndUserId(String uri, Long id) {
        cartRepository.deleteByUriAndUserId(uri, id);
    }

    public List<Cart> getCartByUserId(Long userId) {
        return Lists.newArrayList(cartRepository.findByUserId(userId));
    }

    public Boolean isInCart(String uri, Long userId) {
        Optional<Cart> cart = cartRepository.findByUriAndUserId(uri, userId);
        return cart.isPresent();
    }
}
