package hu.unideb.rft.service;

import hu.unideb.rft.model.Favorite;
import hu.unideb.rft.repository.FavoriteRepository;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FavoriteService {
    private final FavoriteRepository favoriteRepo;

    @Autowired
    public FavoriteService(FavoriteRepository favoriteRepo) {
        this.favoriteRepo = favoriteRepo;
    }

    public void addFavorite(Favorite f) {
        Optional<Favorite> fav = favoriteRepo.findByUriAndUserId(f.getUri(), f.getUserId());
        if(!fav.isPresent()) {
            favoriteRepo.save(f);
        }
    }

    public void deleteById(Long id) {
        favoriteRepo.deleteById(id);
    }

    public void deleteByUriAndUserId(String uri, Long id) {
        favoriteRepo.deleteByUriAndUserId(uri, id);
    }

    public List<Favorite> getFavoritesByUserId(Long userId) {
        return Lists.newArrayList(favoriteRepo.findByUserId(userId));
    }

    public Boolean isFavorite(String uri, Long userId) {
        Optional<Favorite> fav = favoriteRepo.findByUriAndUserId(uri, userId);
        return fav.isPresent();
    }
}
