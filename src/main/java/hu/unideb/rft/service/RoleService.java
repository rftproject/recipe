package hu.unideb.rft.service;

import hu.unideb.rft.model.Role;

import hu.unideb.rft.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
    
     public Role findByRoleName(String name) {
        if (name != null) 
            return roleRepository.findByRoleName(name);
        return null;
    }
     
     public Role findByRoleId(Long id) {
        if (id != null) 
            return roleRepository.findByRoleId(id);
        return null;
    }
}
