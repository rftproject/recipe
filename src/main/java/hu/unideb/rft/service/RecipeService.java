package hu.unideb.rft.service;

import hu.unideb.rft.api.RestClientService;
import hu.unideb.rft.api.entity.Recipe;
import hu.unideb.rft.exception.BusinessException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RecipeService {

    private final RestClientService clientService;

    @Autowired
    public RecipeService(RestClientService clientService) {
        this.clientService = clientService;
    }

    public List<Recipe> getRecipes(String queryParams, int from, int to) {
        try {
            return clientService.downloadRecipes(queryParams, from, to);
        } catch (BusinessException e) {
            log.error("Download recipes failed", e);
            return null;
        }
    }

    public Recipe getRecipeById(String uri) {
        try {
            return clientService.getRecipeById(uri);
        } catch (BusinessException e) {
            log.error("Get recipe by ID failed", e);
            return null;
        }
    }
}
