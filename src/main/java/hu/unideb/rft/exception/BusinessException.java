package hu.unideb.rft.exception;

/**
 * Üzleti logikai kivétel.
 */
public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }

    /**
     * Láncolt kivétel üzenettel.
     *
     * @param message hibaüzenet
     * @param cause láncolt kivétel
     */
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Láncolt kivétel.
     *
     * @param cause láncolt kivétel
     */
    public BusinessException(Throwable cause) {
        super(cause);
    }
}
